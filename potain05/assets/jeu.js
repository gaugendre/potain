"use strict";

const GAME_NOT_STARTED = 0;
const GAME_STARTED = 1;
const GAME_WON = 2;
const GAME_LOST = 3;
const CHEAT = false;

let gameState = GAME_NOT_STARTED;

let zones = undefined;
let timer = undefined;

let endTimeout = undefined;
let roundTimeout = undefined;
let timerInterval = undefined;

let timeBetween = 2000;
const MIN_TIME_BETWEEN = 1000;
const ACCELERATION = 100;
const DURATION = 60_500;

let score = 0;
let zonesCount = 0;
let zonesTotal = 0;
let startTime = undefined;

function start() {
    document.getElementsByTagName("button")[0].disabled = true;
    gameState = GAME_STARTED;
    startTime = new Date();
    timer = document.getElementById("timer");

    zones = document.getElementsByClassName("zone");
    for (const zone of zones) {
        zone.addEventListener("click", event => {
            if (gameState !== GAME_STARTED) {
                return;
            }
            if (!event.target.classList.contains("inactive")) {
                event.target.style.transition = "";
                event.target.classList.remove("fade-out");
                event.target.classList.add("inactive");
                incrementScore();
            }
        })
    }

    endTimeout = setTimeout(end, DURATION);
    timerInterval = setInterval(updateTimer, 500);
    roundTimeout = setTimeout(round, 1);
}

function updateTimer() {
    const now = new Date();
    timer.innerText = Math.floor(Math.abs(startTime - now) / 1000).toString();
}

function round() {
    if (gameState !== GAME_STARTED) {
        end();
        return;
    }

    let zone = zones[Math.floor(Math.random() * zones.length)];

    incrementTotal();

    zone.classList.remove("inactive", "fade-out");
    zone.style.transition = "";

    if (CHEAT) {
        setTimeout(() => {
            zone.click();
        }, 200);
    }

    setTimeout(function() {
        zone.style.transition = `opacity ${timeBetween-(MIN_TIME_BETWEEN / 2)}ms linear`;
    }, 100);

    setTimeout(function () {
        zone.classList.add("fade-out");
    }, MIN_TIME_BETWEEN / 2);

    // Recursive call, sorta
    timeBetween -= ACCELERATION;
    if (timeBetween <= MIN_TIME_BETWEEN) {timeBetween = MIN_TIME_BETWEEN;}
    roundTimeout = setTimeout(round, timeBetween);
}

function end() {
    clearTimeout(roundTimeout);
    clearTimeout(endTimeout);
    clearInterval(timerInterval);
    if (gameState === GAME_STARTED) {
        gameState = GAME_WON;
        document.getElementsByClassName("success stamp")[0].classList.remove("hidden");
    } else if (gameState === GAME_LOST) {
        document.getElementsByClassName("failure stamp")[0].classList.remove("hidden");
    }
}

function incrementScore() {
    score += Math.floor(Math.random() * zones.length) + 1;
    zonesCount++;
    document.getElementById("score").innerText = score.toString();
    document.getElementById("zonesCount").innerText = zonesCount.toString();
}

function incrementTotal() {
    zonesTotal++;
    document.getElementById("zonesTotal").innerText = zonesTotal.toString();
}
